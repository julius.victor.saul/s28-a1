// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API
fetch('https://jsonplaceholder.typicode.com/todos/')

// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
// ditch the parenthesis if you have only 1 parameter in an arrow functon
.then(resp => resp.json())
.then(json => console.log(json.map(obj => obj.title)));


// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1')
// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
.then(resp => resp.json())
.then(json => console.log(`The item "${json['title']}" on the list has a status of ${json['completed']}`));


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        userId: 1,
        id: 201,
        title: "Created To Do List Item",
        completed: false
    })
})
.then(resp => resp.json())
.then(json => console.log(json));


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
// 9. Update a to do list item by changing the data structure.
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        title: "Updated To Do List Item",
        description: "To update the my to do list with a different data structure",
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1
    })
})
.then(resp => resp.json())
.then(json => console.log(json));


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PATCH',
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
        status: "Complete",
        dateCompleted: "07/09/21"
    })
})
.then(resp => resp.json())
.then(json => console.log(json));


// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
});
